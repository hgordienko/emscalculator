# Калькулятор почтовых отправлений

Показывает значение стоимости и сроков доставки EMS-отправления.

API: http://www.emspost.ru/ru/corp_clients/dogovor_docements/api/

Программа реализована на Java 7.

Для GUI был использован Swing.

* возможность выбора пункта отправления и пункта назначения
* возможность задать вес и тип посылки ( в случае международных отправлений - DOC или ATT )

Текущая версия EMSCalculator_1.0.jar

## Использование

* java -jar EMSCalculator_1.0.jar

## https://bitbucket.org/hgordienko/emscalculator/downloads