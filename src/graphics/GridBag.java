package graphics;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

import emscalculator.Client;
import emscalculator.PackageType;
import emscalculator.LocationType;

import javax.swing.JFormattedTextField;

import org.json.JSONObject;

public class GridBag implements ActionListener, PropertyChangeListener {
	
	private Client proxy;
	
	private JComboBox<Location> fromBox;
	private JComboBox<Location> toBox;
	private JLabel labelFrom;
	
	private JLabel labelTo;
	
	private JButton calcButton;
	private JButton clearButton;
	private JLabel labelWeight;
	private JFormattedTextField weightField;
	private JComboBox<PackageType> packageType;
	private JLabel labelPackageType;
	private JLabel price;
	private JLabel minTerm;
	private JLabel maxTerm;
	private JLabel warning;

	
	final static boolean shouldFill = true;
    final static boolean shouldWeightX = true;
    final static boolean RIGHT_TO_LEFT = false;
    
    
    public GridBag() {
	     
    	 this.proxy = Client.getInstance();
    	 // getting location names
    	 Location[] locs = this.proxy.getLocations();
    	 
    	 //--------- COMBOBOXES
    	 packageType = new JComboBox <PackageType>( PackageType.values() );
		 fromBox = new JComboBox<Location>( locs );
		 toBox = new JComboBox<Location>( locs );
		 
		 //--------- LABELS
		 labelFrom = new JLabel("FROM");
		 labelTo = new JLabel("TO");
		 labelWeight = new JLabel("WEIGHT"); 
		 labelPackageType = new JLabel("TYPE");
		 price = new JLabel();	
		 minTerm = new JLabel();
		 maxTerm = new JLabel();
		 warning = new JLabel();
	
		 //--------- BUTTONS
		 calcButton = new JButton("Calculate!");
		 clearButton = new JButton("Clear!");
		 
		 //--------- FORMATTED FIELD
		 DecimalFormat nf =  new DecimalFormat("#0.00");;
		 nf.setDecimalSeparatorAlwaysShown(true);
		 weightField  = new JFormattedTextField(nf);
		 weightField.setValue( 0.01 );
		 weightField.addPropertyChangeListener("value", this);
		 
		 javax.swing.SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
	                createAndShowGUI();
	            }
	    });
	}

    
    public void addComponentsToPane(Container pane) {
    	
        if (RIGHT_TO_LEFT) {
            pane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        }
       
        pane.setLayout( new GridBagLayout() );
        GridBagConstraints constr = new GridBagConstraints();
        if (shouldFill) {
        	
        	constr.fill = GridBagConstraints.HORIZONTAL;
        }
     
      
        if (shouldWeightX) {
        	constr.weightx = 0.5;
        }
        
        //---------------------- LABEL FROM
        constr.gridx = 0;
        constr.gridy = 0;
        constr.insets = new Insets(10,10,0,10); 
        pane.add(this.labelFrom, constr);
        
        //----------------------LABEL TO
        constr.gridy = 1;
        pane.add(this.labelTo, constr);
        
        //---------------------- LABEL TYPE
        constr.gridy = 2;
        pane.add(labelPackageType, constr);
        
        //---------------------- LABEL WEIGHT
        constr.insets = new Insets(10,10,10,10); 
        constr.gridx = 0;
        constr.gridy = 3;
        pane.add(labelWeight, constr);
        
        //---------------------- LOCATION TYPE (FROM)
        constr.weightx = 0.5;
        constr.gridx = 1;
        constr.gridy = 0;
        constr.gridwidth = 2;
        constr.insets = new Insets(10,0,0,10); 
        pane.add(this.fromBox, constr);
 
        //---------------------- LOCATION TYPE (TO)  
        constr.gridy = 1;
        pane.add(this.toBox, constr);
        
        //---------------------- TYPE FIELD
        constr.gridy = 2;
        pane.add(packageType, constr);
        
        //---------------------- WEIGHT FIELD
      	constr.insets = new Insets(0,0,0,10); 
        constr.gridx = 1;
        constr.gridy = 3;
        constr.gridwidth = 1;   
        pane.add(weightField, constr);
        
        //---------------------- CALCULATE BUTTON
        calcButton.setActionCommand("CALC");
        calcButton.addActionListener( this );
        constr.insets = new Insets(0,10,0,10); 
    	constr.weighty = 1.0;  
     	constr.weightx = 0.0;  
        constr.ipady = 40;
        constr.gridx = 0;
        constr.gridy = 4;
        constr.gridwidth = 7;
        pane.add(this.calcButton, constr);
          
        //---------------------- CLEAR BUTTON  
        clearButton.setActionCommand("CLEAR");
        clearButton.addActionListener( this );
        constr.insets = new Insets(10,0,15,10); 
        constr.fill = GridBagConstraints.VERTICAL;
        constr.gridx = 6;
        constr.gridy = 0;
        constr.ipady = 100;
        constr.gridheight = 4;
        constr.gridwidth = 1;
        pane.add(clearButton, constr);
        
   
        // ----------------------- PRICE LABEL
        constr.fill = GridBagConstraints.HORIZONTAL;
        constr.ipady = 10;
        constr.weightx = 0.5;
        constr.weighty = 0.5;
        constr.insets = new Insets(10,10,10,10); 
        constr.gridx = 0;
        constr.gridy = 5;
        constr.gridwidth = 2;
        constr.gridheight = 1;
        pane.add(this.price, constr);
        
        // ----------------------- MIN TERM LABEL
        constr.gridx = 0;
        constr.gridwidth = 2;
        constr.gridy = 6;
        pane.add(this.minTerm, constr);
        
        // ----------------------- MAX TERM LABEL
        constr.gridy = 7;
        pane.add(this.maxTerm, constr);
        
        // ----------------------- WARNING LABEL
        constr.fill = GridBagConstraints.NONE;
        constr.ipady = 40;
        constr.weightx = 0.5;
        constr.gridx = 0;
        constr.gridy = 8;
        constr.insets = new Insets(0,10,0,10); 
        constr.gridwidth = 7;
        pane.add(this.warning, constr);
        
    }
 
    private void createAndShowGUI() {
       
        JFrame frame = new JFrame("EMS Calculator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(650, 400);
        frame.setMaximumSize(new Dimension(650, 400));
        frame.setMinimumSize(new Dimension(650, 400));
        //Set up the content pane.
        addComponentsToPane(frame.getContentPane());
 
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
   
    // update proxy members and text fields before calculating
	private void updateFields() {
		
		proxy.setF( ( (Location) (this.fromBox.getSelectedItem())).getType() );
    	proxy.setT( ( (Location) (this.toBox.getSelectedItem())).getType() );
    	proxy.setType( (PackageType) this.packageType.getSelectedItem() );
    	proxy.setFrom( ( (Location) (this.fromBox.getSelectedItem())).getName() );
    	proxy.setTo( ( (Location) (this.toBox.getSelectedItem())).getName() );
    	
        proxy.setWeight( ((Number)weightField.getValue()).doubleValue());
        proxy.CheckWeight();
        warning.setText(proxy.getWarning());
        // checking fields Weight, PackageType
        this.weightField.setValue( proxy.getWeight() );
        this.packageType.setSelectedItem( proxy.getType() );
        
        // clear output labels in case they are not empty
        price.setText("");
		minTerm.setText("");
		maxTerm.setText("");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		String command = e.getActionCommand(); 
		
        if( command.equals( "CALC" ))  {
        	
        	updateFields();
        	try {
				if ( !proxy.CheckTo() ){
					this.warning.setText( proxy.getWarning() );
					// TODO: warn user via log
					return;
				}
				
				if ( !proxy.CheckFrom() ){
					this.warning.setText(  proxy.getWarning() );
					// TODO: warn user via log
					return;
				}
					
			} catch (Exception e1) {
				e1.printStackTrace();
			}
        	
        	warning.setText("");
        	JSONObject rsp = proxy.calcResult();
        	
        	// parsing response from server
        	if ( rsp.has("stat") ) {
        		if ( rsp.get("stat").equals("ok") ) {
        			
        			price.setText("price=" + rsp.getString("price"));
        			
        			if (rsp.has("term")) {
					
        				JSONObject term = rsp.getJSONObject("term");
        				minTerm.setText("min (days) =" + term.getString("min") );
        				maxTerm.setText("max (days) =" + term.getString("max") );
					
        			}
				
        		} else {
        			// if error occurred, printing error message
        			JSONObject err =  rsp.getJSONObject("err") ;
        			System.out.println(rsp);
        			this.warning.setText( err.getString("msg"));
    				
        		}
        	
        	} else {
        		
        			this.warning.setText( rsp.toString() );
        	}
        	
        	
        	
        } else {
        	
			proxy.setF(LocationType.CITIES);
			this.fromBox.setSelectedIndex(0);
			
			proxy.setT(LocationType.CITIES);
			this.toBox.setSelectedIndex(0);
			
			proxy.setWeight(0.01);
			this.weightField.setValue((Number)0.01);
			
			proxy.setType(PackageType.NA);
		    this.packageType.setSelectedIndex(0);
		    
		    this.warning.setText("");
		    
		    price.setText("");
			minTerm.setText("");
			maxTerm.setText("");
			
			System.out.println("Info cleaned");
		}
		
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		Object source = evt.getSource();
        if (source == weightField) {
        	proxy.setWeight( ((Number)weightField.getValue()).doubleValue() );
        }
		
	}

}
