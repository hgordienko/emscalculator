package graphics;

import emscalculator.LocationType;

public class Location {
	private LocationType type;
	//private String type;
	private String name;
	
	public Location( LocationType lt, String name ) {
		this.type = lt;
		this.name = new String(name);
	}
	
	public Location( String lt, String name ) {
		
		if ( lt.equalsIgnoreCase("cities") )
			this.type = LocationType.CITIES;
		else if ( lt.equalsIgnoreCase("regions") )
			this.type = LocationType.REGIONS;
		else 
			this.type = LocationType.COUNTRIES;
		
		this.name = new String(name);
	}
	
	public LocationType getType() {
		return type;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return this.getType().Name() + ", " + this.name;
		
	}
}
