package emscalculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

public class APImethod {
	private String host;
	private URL url;
	
	public APImethod() {
		
		host = new String("http://emspost.ru/api/rest/?method=");
		
	}
	
	
	public double GetMaxWeight() throws Exception {
		
		String result = ExecuteQuery( "ems.get.max.weight" );
		
		JSONObject rsp = new JSONObject(result).getJSONObject("rsp");
		
		return rsp.getDouble("max_weight");
	}
	
	public JSONArray GetLocations( LocationType type, boolean plain ) throws Exception {
		String prefix = new String("ems.get.locations");
		String typeName = type.Name();
		
		String result = ExecuteQuery( prefix + "&" + "type=" + typeName + "&" + "plain=" + plain );
		
		JSONObject rsp = new JSONObject(result).getJSONObject("rsp");
		JSONArray locs = rsp.getJSONArray("locations");
		return locs;
	}
	
	// arguments are considered to be valid
	// from, to 
	// city--NAME or region--NAME or CN ( country name )
	public String Calculate( String from, String to, double weight, PackageType packageType ) throws Exception {
		String prefix = new String("ems.calculate");
		
		from = from.equals("")? from : new String("&from=") + from;
		
		String packageTypeString = packageType.Name();;
		
		if ( ! packageTypeString.equalsIgnoreCase("") ) {
			packageTypeString = new String("&type=") + packageTypeString;
		}
		
		return ExecuteQuery( prefix + from + "&" + "to=" + to + "&" + "weight=" + new Double(weight).toString() + packageTypeString);
	}
	

	public String ExecuteQuery(String query) throws Exception  {
		String resultString = "";
		
		
		try {
			
            url = new URL(host + query);
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String line;
          
            while ((line = reader.readLine()) != null) {
            	resultString += line;

            }
           
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
        	e.printStackTrace();
        }
		
		return resultString;
	}
}
