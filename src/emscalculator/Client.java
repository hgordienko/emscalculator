package emscalculator;


import graphics.Location;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

// executes queries ( GET-method )
public class Client {
	private static Client instance = new Client();
	private static Logger log = Logger.getLogger(Client.class.getName());
	private String warning;
	private String from, to; // from, to - names
	private LocationType f, t;
	private double weight;
	private PackageType type;
	private APImethod api;
	
	
	
	private Client( String from, LocationType f, String to, LocationType t, double weight, PackageType type ) {
		this.from = from;
		this.f = f;
		this.to = to;
		this.t = t;
		this.weight = weight;
		this.type = type;
		this.api = new APImethod();
		this.warning = new String("");

	}
	
	private Client() {
		this("", LocationType.CITIES, "", LocationType.CITIES, 0.00, PackageType.NA);
	}
	
	public static Client getInstance() {
		return instance;
	}
	// checks whether weight is valid
	// NOTE: type of package can be modified in the process
	// 			if it exceeds maxDocWeight package is marked as ATT

	public void CheckWeight() {
	    final double maxDocWeight = 2.00;
	    
		if ( Double.compare(weight, 0.00) <= 0 ) {
			weight = Math.abs( weight );
			warning = "Invalid weight: weight cannot be negative. ";
			log.info(warning);
		}
		
		// if the delivery is international
		if ( this.t.equals(LocationType.COUNTRIES) ) {		
			// checks whether weight is larger than maxDocWeight
			// if it is - switches type to PackageType.ATT
			 if ( Double.compare(maxDocWeight, weight) < 0 ) {
				 type = PackageType.ATT;
			 } else {
				 type = PackageType.DOC;
			 }
		} else {
			type = PackageType.NA;
		}
		
		try {
			double maxWeight = api.GetMaxWeight();
			
			if ( Double.compare(weight, maxWeight) > 0 ) {
				warning = "Invalid weight: weight cannot exceed " + Double.toString(maxWeight) + "kg";
				log.info( warning );
				weight = maxWeight;
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, "Exception: ", e);
			e.printStackTrace();
			
		}
	}
	
	private Location[] JSONtoLocation( JSONArray jArr ) {
		
		Location[] result = new Location[ jArr.length() ];	
		for ( int i = 0; i < jArr.length(); i++) {
			
			JSONObject nextToken = jArr.getJSONObject(i);
		
			if ( nextToken.has( "name" ) && nextToken.has( "type" ) ) {
				
				result[i] = new Location( nextToken.getString("type"), nextToken.getString("name"));
			
			}
			
		}
		return result;
	}
	
	public Location[] getLocations() {
		
		Location[] result = new Location[1];
		Location empty = new Location(LocationType.CITIES, "NONE");
		
		try {
			
			int count = 0;
			JSONArray locs = api.GetLocations(LocationType.CITIES, true);
			count += locs.length();
			Location[] cities = JSONtoLocation(locs);
			
			locs = api.GetLocations(LocationType.REGIONS, true);
			count += locs.length();
			Location[] regions = JSONtoLocation(locs);
		
			locs = api.GetLocations(LocationType.COUNTRIES, true);
			count += locs.length();
			Location[] countries = JSONtoLocation(locs);
			
			result = new Location[count + 1];
			System.arraycopy(cities, 0, result, 1, cities.length );
			System.arraycopy(regions, 0, result, cities.length + 1, regions.length );
			System.arraycopy(countries, 0, result, cities.length  + regions.length + 1, countries.length );
				
		} catch (Exception e) {
			log.log(Level.SEVERE, "Exception: ", e);
			e.printStackTrace();
		}
		result[0] = empty;
		return result;
	} 
	
	// finds location by its name and type in JSONArray response
	private JSONObject findLocationByName( String name, LocationType type, JSONArray jArr ) {
		
		boolean found = false;
		JSONObject target = new JSONObject();
		
		int i = 0;
		while (  i < jArr.length()  ) {
			JSONObject nextToken = jArr.getJSONObject(i);
		
			if ( nextToken.has( "name" ) && nextToken.has( "type" ) ) {
				found = name.equalsIgnoreCase(nextToken.getString("name")) && type.Name().equalsIgnoreCase(nextToken.getString("type"));
				if ( found ) {
					target = nextToken;
					break;
				}
			}
			i++;
		}
		
		return target;
		
	}
	
	// TODO: wrap CheckFrom() and CheckTo() into one function
	// checks whether location names ( from ) are valid and translates them into values:
	// city--NAME, region--NAME, ?? (country name)
	public boolean CheckFrom() throws Exception {
		
		// if <from> is empty, check whether it's delivery to another country
		if ( (this.from).equalsIgnoreCase("NONE") ) {
			warning = "field <from> is empty";
			log.info(warning);
			return this.t.equals(LocationType.COUNTRIES);
		}
		
		if ( (this.f).equals(LocationType.COUNTRIES) ) {
			warning = "field <from> cannot be a foreign country";
			log.info(warning);
			return false;
		}
		
		// if <from> isn't empty, check whether it's valid
		JSONArray result = api.GetLocations(f, true); // plain==false stands for Unicode encoded 
		JSONObject target = findLocationByName( this.from, this.f, result );
		
		if ( target.length() == 0 ) {
			warning = "Invalid location: name="+ this.from + " type=" + this.f;
			log.info(warning);
			return false;
		} else {
			this.from = target.getString("value");  // renaming location
			
		}
		
		 return true; // if everything is OK
	}
	
	// checks whether location names ( to ) are valid and translates them into values:
	public boolean CheckTo() throws Exception {
		
		if ( (this.to).equalsIgnoreCase("NONE") ) {
			warning = "Invalid location: field <to> is empty";
			log.info( warning );
			return false;
		}
		
		JSONArray result = api.GetLocations(t, true); // plain==false stands for Unicode encoded 
		JSONObject target = findLocationByName( this.to, this.t, result );
	
		if ( target.length() == 0  ) {
			warning = "Invalid location" ;
			log.info(warning  + ": name=" + this.to + " type=" + this.t);
			return false;
		} else {
			this.to = target.getString("value"); // renaming location
			
		}
		
		return true; // if everything is OK
	}
	
	
	// calculates price and terms
	public JSONObject calcResult() {
		JSONObject rsp = new JSONObject();
		
		System.out.println("Calculating!");
		try {
		
			// trying to calculate price and terms
			String result = api.Calculate(from, to, weight, type);
			rsp = new JSONObject(result).getJSONObject("rsp");
						
		} catch (Exception e) {
			log.log(Level.SEVERE, "Exception: ", e);
			e.printStackTrace();
		}
		System.out.println(rsp);
		return rsp;
	}

	//---------- setters
	
	public void setFrom(String from) {
		this.from = from;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public void setF(LocationType f) {
		this.f = f;
	}

	public void setT(LocationType t) {
		this.t = t;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public void setType(PackageType type) {
		this.type = type;
	}
	
	//----------- getters
	
	public double getWeight() {
		return this.weight;
	}
	
	public PackageType getType() {
		return this.type;
	}
	
	// get warning and reset it
	public String getWarning() {
		String w = new String(warning);
		warning = "";
		return w;
	}
}
