package emscalculator;

// DOC  - packages weighing less than 2 kg ( international delivery )
// ATT  - heavier packages ( international delivery )
// NA	- used for local delivery
public enum PackageType {
	NA {
		public String Name() { return new String(""); }
	}, DOC {
		public String Name() { return new String("doc"); }
	}, ATT {
		public String Name() { return new String("att"); }
	};
	
	public abstract String Name();
}
