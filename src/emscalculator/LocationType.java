package emscalculator;

// location types for GETLOCATIONS method
public enum LocationType {
	CITIES {
		public String Name() { return new String("cities"); }
	}, REGIONS {
		public String Name() { return new String("regions"); }
	}, COUNTRIES {
		public String Name() { return new String("countries"); }
	};
	
	public abstract String Name();
}
